$(() => {
    $(".tabs-title").on("click", function (){
        $(".tabs-title").removeClass("active");
        $(this).addClass("active")
        $(".tabs_text").each((index, element)=>{
            if ($(element).attr("data-text-id") === $(this).attr("data-text-id")){
                $(element).addClass("active");
            }
            else {
                $(element).removeClass("active");
            }
        })
    });
})
